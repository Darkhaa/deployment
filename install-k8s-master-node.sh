#!/usr/bin/bash

echo ""
echo ' ________  ______  _______    ______          ______   __        ______   __    __  _______  '
echo '|        \|      \|       \  /      \        /      \ |  \      /      \ |  \  |  \|       \ '
echo '| $$$$$$$$ \$$$$$$| $$$$$$$\|  $$$$$$\      |  $$$$$$\| $$     |  $$$$$$\| $$  | $$| $$$$$$$\'
echo '| $$__      | $$  | $$__/ $$| $$  | $$      | $$   \$$| $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$  \     | $$  | $$    $$| $$  | $$      | $$      | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$$$$     | $$  | $$$$$$$\| $$  | $$      | $$   __ | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$       _| $$_ | $$__/ $$| $$__/ $$      | $$__/  \| $$_____| $$__/ $$| $$__/ $$| $$__/ $$'
echo '| $$      |   $$ \| $$    $$ \$$    $$       \$$    $$| $$     \\$$    $$ \$$    $$| $$    $$'
echo ' \$$       \$$$$$$ \$$$$$$$   \$$$$$$         \$$$$$$  \$$$$$$$$ \$$$$$$   \$$$$$$  \$$$$$$$ '
echo ""
echo ""
echo ""

echo -e "\e[96m------- Initialize the cluster ------- "
tput init
sudo kubeadm init --pod-network-cidr=10.244.0.0/16

echo -e "\e[96m------- Set up local kubeconfig ------- "
tput init
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


echo -e "\e[96m------- Apply Calco CNI ------- "
tput init
kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml
