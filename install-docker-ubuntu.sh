#!/usr/bin/bash
echo -e "\e[96m------- Update the apt package index and install packages to allow apt to use a repository over HTTPS ------- "
tput init
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y

echo -e "\e[96m------- Add Docker’s official GPG key: ------- "
tput init
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo -e "\e[96m ------- Add repository ------- "
tput init
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo -e "\e[96m ------- Update the apt package index, and install the latest version of Docker Engine and containerd, or go to the next step to install a specific version: ------- "
tput init
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
