#!/usr/bin/bash

echo ""
echo ' ________  ______  _______    ______          ______   __        ______   __    __  _______  '
echo '|        \|      \|       \  /      \        /      \ |  \      /      \ |  \  |  \|       \ '
echo '| $$$$$$$$ \$$$$$$| $$$$$$$\|  $$$$$$\      |  $$$$$$\| $$     |  $$$$$$\| $$  | $$| $$$$$$$\'
echo '| $$__      | $$  | $$__/ $$| $$  | $$      | $$   \$$| $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$  \     | $$  | $$    $$| $$  | $$      | $$      | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$$$$     | $$  | $$$$$$$\| $$  | $$      | $$   __ | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$       _| $$_ | $$__/ $$| $$__/ $$      | $$__/  \| $$_____| $$__/ $$| $$__/ $$| $$__/ $$'
echo '| $$      |   $$ \| $$    $$ \$$    $$       \$$    $$| $$     \\$$    $$ \$$    $$| $$    $$'
echo ' \$$       \$$$$$$ \$$$$$$$   \$$$$$$         \$$$$$$  \$$$$$$$$ \$$$$$$   \$$$$$$  \$$$$$$$ '
echo ""
echo ""
echo ""

echo -e "\e[96m------- Get the Kubernetes gpg key ------- "
tput init
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

echo -e "\e[96m------- Add the Kubernetes repository ------- "
tput init
cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

echo -e "\e[96m------- Update your packages ------- "
tput init
sudo apt-get update

echo -e "\e[96m------- Install kubelet, kubeadm, and kubectl ------- "
tput init
sudo apt-get install -y kubelet kubeadm kubectl

echo -e "\e[96m------- Hold them at the current version ------- "
tput init
sudo apt-mark hold docker-ce kubelet kubeadm kubectl

echo -e "\e[96m------- Add the iptables rule to sysctl.conf ------- "
tput init
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf

echo -e "\e[96m------- Enable iptables immediately ------- "
tput init
sudo sysctl -p