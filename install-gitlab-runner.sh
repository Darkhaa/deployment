#!/usr/bin/bash

echo ""
echo ' ________  ______  _______    ______          ______   __        ______   __    __  _______  '
echo '|        \|      \|       \  /      \        /      \ |  \      /      \ |  \  |  \|       \ '
echo '| $$$$$$$$ \$$$$$$| $$$$$$$\|  $$$$$$\      |  $$$$$$\| $$     |  $$$$$$\| $$  | $$| $$$$$$$\'
echo '| $$__      | $$  | $$__/ $$| $$  | $$      | $$   \$$| $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$  \     | $$  | $$    $$| $$  | $$      | $$      | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$$$$     | $$  | $$$$$$$\| $$  | $$      | $$   __ | $$     | $$  | $$| $$  | $$| $$  | $$'
echo '| $$       _| $$_ | $$__/ $$| $$__/ $$      | $$__/  \| $$_____| $$__/ $$| $$__/ $$| $$__/ $$'
echo '| $$      |   $$ \| $$    $$ \$$    $$       \$$    $$| $$     \\$$    $$ \$$    $$| $$    $$'
echo ' \$$       \$$$$$$ \$$$$$$$   \$$$$$$         \$$$$$$  \$$$$$$$$ \$$$$$$   \$$$$$$  \$$$$$$$ '
echo ""
echo ""
echo ""

echo -e "\e[96m------- Simply download one of the binaries for your system: ------- "
tput init
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"

echo -e "\e[96m------- Give it permissions to execute: ------- "
tput init
sudo chmod +x /usr/local/bin/gitlab-runner

echo -e "\e[96m ------- Create a GitLab CI user: ------- "
tput init
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

echo -e "\e[96m ------- Install and run as service: ------- "
tput init
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
